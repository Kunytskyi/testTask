<?php
defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['main'] = 'EXT:main/Configuration/RTE/Default.yaml';


// Configuring page 404
$TYPO3_CONF_VARS['FE']['pageNotFound_handling'] = 'REDIRECT:http://grunne.loc/index.php?id=16';
$TYPO3_CONF_VARS["FE"]["pageNotFound_handling_statheader"] = 'HTTP/1.1 404 Not Found';

// outsource gridelements config to ext_localconf.php
// https://forge.typo3.org/issues/60949
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:main/Configuration/TsConfig/Gridelements/" extensions="ts">'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:main/Configuration/TsConfig/Page/" extensions="ts">'
);
