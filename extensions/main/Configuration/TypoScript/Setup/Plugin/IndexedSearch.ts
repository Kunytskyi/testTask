plugin.tx_indexedsearch {

    view {
        templateRootPaths {
            0 = EXT:indexed_search/Resources/Private/Templates/
            10 = {$plugin.tx_indexedsearch.view.templateRootPath}
        }

        partialRootPaths {
            0 = EXT:indexed_search/Resources/Private/Partials/
            10 = {$plugin.tx_indexedsearch.view.partialRootPath}
        }
    }

    settings {
        rootPidList = 1
        targetPid = 18
        detectDomainRecords = 1
        results.summaryCropAfter = 30
    }

    search {
        targetPid >
        targetPid = 18
    }
}

config.index_enable = 1
page.config.index_enable = 1
config.index_externals = 1
