tt_content.gridelements_pi1 = COA
tt_content.gridelements_pi1.20 = COA
tt_content.gridelements_pi1.20.10.setup {

    OneColumn < lib.gridelements.defaultGridSetup
    OneColumn {
        # FLUIDTEMPLATE konfigurieren
        cObject = FLUIDTEMPLATE
        cObject {
            file = EXT:main/Resources/Private/Templates/ContentElements/Gridelements/one_column.html
        }
    }

    TwoColumns < lib.gridelements.defaultGridSetup
    TwoColumns {
        cObject = FLUIDTEMPLATE
        cObject {
            file = EXT:main/Resources/Private/Templates/ContentElements/Gridelements/two_columns.html
        }
    }

    ThreeColumns < lib.gridelements.defaultGridSetup
    ThreeColumns {
        cObject = FLUIDTEMPLATE
        cObject {
            file = EXT:main/Resources/Private/Templates/ContentElements/Gridelements/three_columns.html
        }
    }

    Sidebar < lib.gridelements.defaultGridSetup
    Sidebar {
        cObject = FLUIDTEMPLATE
        cObject {
            file = EXT:main/Resources/Private/Templates/ContentElements/Gridelements/sidebar.html
        }
    }

    Accordion < lib.gridelements.defaultGridSetup
    Accordion {
        cObject = FLUIDTEMPLATE
        cObject {
            file = EXT:main/Resources/Private/Templates/ContentElements/Gridelements/accordion.html
        }
    }

    Slider < lib.gridelements.defaultGridSetup
    Slider {
        cObject = FLUIDTEMPLATE
        cObject {
            file = EXT:main/Resources/Private/Templates/ContentElements/Gridelements/slider.html
        }
    }
}
