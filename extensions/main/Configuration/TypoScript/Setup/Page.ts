page = PAGE
page {
    typeNum = 0

    # Meta tags Configuring
    meta {
        X-UA-Compatible = IE=edge
        viewport        = width=device-width, initial-scale=1

        keywords.field    = keywords
        abstract.field    = abstract
    }

    10 = FLUIDTEMPLATE

    10 {

        templateName = TEXT
        templateName.stdWrap {
            cObject = CASE
            cObject {
                key.data = pagelayout
                key.ifEmpty.data = levelfield:-2, backend_layout_next_level, slide
                pagets__main = TEXT
                pagets__main.value = Main
                pagets__internal = TEXT
                pagets__internal.value = Internal
                pagets__article = TEXT
                pagets__article.value = Article
                default = TEXT
                default.value = Default
            }
        }

        partialRootPaths {
            0 = EXT:main/Resources/Private/Partials/
        }

        templateRootPaths {
            0 = EXT:main/Resources/Private/Templates/Page/
        }

        layoutRootPaths {
            0 = EXT:main/Resources/Private/Layouts/
        }

        # Configuring header rendering
        dataProcessing {
            10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
            10 {
                references.fieldName = media
            }
            20 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            20 {
                levels = 1
                includeSpacer = 1
                as = mainnavigation
            }
        }
    }

    # Adding bootstrap 4
    includeCSSLibs {
        bootstrap = https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css
    }

    includeJSFooterlibs {
        Jquery = https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
        Pooper = https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js
        minJs = https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js
    }

}
