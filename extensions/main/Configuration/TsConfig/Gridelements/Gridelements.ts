tx_gridelements {
    overruleRecords = 1
}

tx_gridelements.setup {

    OneColumn {
        title = One column container
        description = Onecolumn container
        topLevelLayout = 1

        config {
            colCount = 1
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = One column
                            colPos = 11
                            allowed = text
                            CType = text
                        }
                    }
                }
            }
        }
    }

    TwoColumns {
        title = Two columns container
        description = Two columns container
        topLevelLayout = 1

        config {
            colCount = 2
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name =Left column
                            colPos = 11
                            allowed = textmedia
                        }
                        2 {
                            name = Right column
                            colPos = 12
                            allowed = textmedia
                        }
                    }
                }
            }
        }
    }

    ThreeColumns {
        title = Three columns container
        description = Three columns container
        topLevelLayout = 1

        config {
            colCount = 3
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name =Left column
                            colPos = 11
                            allowed = textmedia
                        }
                        2 {
                            name = Center column
                            colPos = 12
                            allowed = textmedia
                        }
                        3 {
                            name = Right column
                            colPos = 13
                            allowed = textmedia
                        }
                    }
                }
            }
        }
    }

    Sidebar {
        title = Sidebar grid element
        description = Sidebar grid element
        topLevelLayout = 1

        config {
            colCount = 2
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Sidebar container
                            colPos = 14
                            allowed = text
                        }
                        2 {
                            name = Text column
                            colPos = 15
                            allowed = textmedia
                        }
                    }
                }
            }
        }
    }

    Accordion {
        title = Accordion grid element
        description = Accordion grid element
        topLevelLayout = 1

        config {
            colCount = 1
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Accordion container
                            colPos = 14
                            allowed = textmedia
                        }
                    }
                }
            }
        }
    }

    Slider {
        title = Slider grid element
        description = Slider grid element
        topLevelLayout = 1

        config {
            colCount = 1
            rowCount = 1
            rows {
                1 {
                    columns {
                        1 {
                            name = Slider container
                            colPos = 14
                            allowed = textmedia
                        }
                    }
                }
            }
        }
        flexformDS = FILE:EXT:main/Configuration/Flexforms/flexform_gridelements.xml
    }
}
