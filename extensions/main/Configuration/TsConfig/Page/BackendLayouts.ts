mod.web_layout.BackendLayouts {
    main {
        title = Main Page
        config {
            backend_layout {
                colCount = 1
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = One column container
                                colPos = 0
                                colspan = 1
                            }
                        }
                    }
                }
            }
        }

    }

    internal {
        title = Internal Page
        config {
            backend_layout {
                colCount = 2
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = 3/4
                                colPos = 0
                                colspan = 3
                            }
                            2 {
                                name = 1/4
                                colPos = 1
                                colspan = 1
                            }
                        }
                    }
                }
            }
        }
    }

    article {
        title = News Article
        config {
            backend_layout {
                colCount = 2
                rowCount = 1

                rows {
                    1 {
                        columns {
                            1 {
                                name = first column
                                colPos = 0
                                colspan = 1
                            }
                        }
                    }
                    2 {
                        columns {
                            1 {
                                name = second column
                                colPos = 1
                                colspan = 1
                            }
                        }
                    }
                }
            }
        }
    }
}
