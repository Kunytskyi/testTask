<?php

return [
    'ctrl' => [
        'title' => 'Team',
        'label' => 'title',
    ],
    'columns' => [
        'title' => [
            'exclude' => true,
            'label' => 'Team name',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim',
            ],
        ],
    ],
    'types' => [
        '0' => [
            'showitem' => 'title',
        ],
    ]
];
