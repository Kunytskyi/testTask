<?php

return [
    'ctrl' => [
        'title' => 'Skills',
        'label' => 'title',
    ],
    'columns' => [
        'title' => [
            'exclude' => true,
            'label' => 'Skills name',
            'foreign_table' => 'tx_employee_domain_model_person',
            'foreign_field' => 'skills',
            'MM' => 'tx_employee_domain_model_skills_mm',

            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim',
            ],
        ],
    ],
    'types' => [
        '0' => [
            'showitem' => 'title',
        ],
    ]
];
