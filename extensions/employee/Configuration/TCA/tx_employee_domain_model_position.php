<?php

return [
    'ctrl' => [
        'title' => 'Position',
        'label' => 'title',
    ],
    'columns' => [
        'title' => [
            'exclude' => true,
            'label' => 'Position name',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim',
            ],
        ],
    ],
    'types' => [
        '0' => [
            'showitem' => 'title',
        ],
    ]
];
