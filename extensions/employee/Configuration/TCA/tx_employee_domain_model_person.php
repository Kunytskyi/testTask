<?php

return [
    'ctrl' => [
        'title' => 'Person',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
    ],
    'columns' => [
        'title' => [
            'exclude' => true,
            'label' => 'Title',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim',
            ],
        ],
        'name' => [
            'exclude' => true,
            'label' => 'Person name',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim',
            ],
        ],
        'isnew' => [
            'exclude' => true,
            'label' => 'New employee',
            'config' => [
                'type' => 'check',
                'default' => true,
            ]
        ],
        'photo' => [
            'exclude' => 1,
            'label' => 'photo',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'images',
                [
                    'appearance' => [
                        'headerThumbnail' => [
                            'width' => '120c',
                            'height' => '150c',
                        ],
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette',
                        ],
                    ],
                ],
                'jpg,jpeg,png'
            ),
        ],
        'phone' => [
            'exclude' => true,
            'label' => 'Person phone:',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim',
            ],
        ],
        'email' => [
            'exclude' => true,
            'label' => 'Person email:',
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim',
            ],
        ],
        'description' => [
            'exclude' => true,
            'label' => 'description:',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 6,
                'size' => '255',
                'eval' => 'trim',
                'enableRichtext' => true
            ],
        ],
        'skills' => [
            'exclude' => true,
            'label' => 'skills:',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_employee_domain_model_skills',
                'foreign_field' => 'pid',
                'foreign_table_position' => 'title',
                'maxitems' => 20,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                ],
            ],
        ],
        'team' => [
            'exclude' => true,
            'label' => 'team:',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'size' => 10,
                'maxitems' => 10,

                'foreign_table' => 'tx_employee_domain_model_team',
                'foreign_table_where' => ' ORDER BY tx_employee_domain_model_team.name',
                'MM' => 'tx_employee_domain_model_team_mm',
            ],
        ],
        'position' => [
            'exclude' => true,
            'label' => 'position:',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'size' => 10,
                'maxitems' => 10,

                'foreign_table' => 'tx_employee_domain_model_position',
                'foreign_table_where' => ' ORDER BY tx_employee_domain_model_position.name',
                'MM' => 'tx_employee_domain_model_position_mm',
            ],
        ],
    ],
    'types' => [
        '0' => [
            'showitem' => 'title, name, isnew, photo, phone, email, description, skills, team, position',
        ],
    ]
];
