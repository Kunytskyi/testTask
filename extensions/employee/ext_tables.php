<?php

defined('TYPO3_MODE') || die('Access denied.');

call_user_func(

    function ($extKey){
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Test.'.$extKey,
            'personsList',
            'List of persons to display',
            'EXT:employee/Resources/Public/Icons/favicon.ico'
        );
    }, $_EXTKEY

);

call_user_func(
    function ($extKey){
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Test.'.$extKey,
            'imagesList',
            'Custom employee images to show',
            'EXT:employee/Resources/Public/Icons/favicon.ico'
        );

        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['employee_imageslist'] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
            'employee_imageslist',
            'FILE:EXT:employee/Configuration/FlexForms/flexform_showimages.xml'
        );
    }, $_EXTKEY
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $_EXTKEY,
        'Configuration/TypoScript',
        'employee'
    );
