<?php

$EM_CONF[$_EXTKEY] = [
   'title' => 'Employee List',
   'description' => 'An extension to manage an employee list.',
   'category' => 'plugin',
   'author' => 'Ihor',
   'author_company' => 'agiliway',
   'author_email' => 'agiliway@example.com',
   'state' => 'stable',
   'clearCacheOnLoad' => true,
   'version' => '1.0.0',
   'constraints' => [
      'depends' => [
         'typo3' => '8.7.0-8.9.99',
         'fluid_styled_content' => '8.7.0-9.5.99',
      ]
   ],
   'autoload' => [
      'psr-4' => [
         'Test\\Employee\\' => 'Classes'
      ]
   ],
];
