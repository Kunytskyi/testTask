<?php
defined('TYPO3_MODE') || die();


call_user_func(
    function ($extKey) {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Test.'.$extKey,
            'personsList',
            [
                'Person' => 'list, show',
            ],
            [
                'Person' => 'list, show',
            ]
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Test.'.$extKey,
            'imagesList',
            [
                'Person' => 'showImages,',
            ],
            [
                'Person' => 'showImages',
            ]
        );
    }, $_EXTKEY
);

// set ajax get/post site
$TYPO3_CONF_VARS['FE']['eID_include']['employeeDetailed'] = 'EXT:employee/Classes/Controller/PersonController.php';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['employeeDetailed'] = Test\Employee\Controller\PersonController::class . '::showAction';

// Setting scheduler
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Test\\Employee\\Task\\UnsetNew'] = array(
        'extension' => $_EXTKEY,
        'title' => 'Unset New User',
        'description' => 'Task for unusetting new users',
        // 'additionalFields' => \TYPO3\CMS\Scheduler\Task\CachingFrameworkGarbageCollectionAdditionalFieldProvider::class
);
