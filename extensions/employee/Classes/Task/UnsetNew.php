<?php

namespace Test\Employee\Task;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class UnsetNew extends \TYPO3\CMS\Scheduler\Task\AbstractTask
{
    public function execute() {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_employee_domain_model_person');
        $queryBuilder
            ->update('tx_employee_domain_model_person')
            ->where(
                $queryBuilder->expr()->lte('crdate', strval(time() - $this->settings['timeframe']))
            )
            ->set('isnew', '0')
            ->execute();
    }
}
