<?php
namespace Test\Employee\Controller;


use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Core\Database\ConnectionPool;

class PersonController extends ActionController
{
    /**
     * documentRepository.
     *
     * @var Test\Employee\Domain\Repository\PersonRepository
     * @inject
     */
    protected $personRepository = null;

    /**
     * documentRepository.
     *
     * @var Test\Employee\Domain\Repository\TeamRepository
     * @inject
     */
    protected $teamRepository = null;

    /**
     * documentRepository.
     *
     * @var Test\Employee\Domain\Repository\PositionRepository
     * @inject
     */
    protected $positionRepository = null;

    /**
     * documentRepository.
     *
     * @var Test\Employee\Domain\Repository\SkillsRepository
     * @inject
     */
    protected $skillsRepository = null;

    /**
     * @var \TYPO3\CMS\Extbase\Service\ImageService
     * @inject
     */
    protected $imageService;

    public function listAction()
    {
        $content = [];
        $persons = $this->personRepository->findAll();
        if($persons){
            foreach ($persons as $key => $value) {
                $person = [
                    'name' => $value->getTitle(),
                    'isNew' => $value->getIsnew(),
                    'photo' => $this->getFigure($value->getPhoto()),
                    'team' => $this->getTeamName($value->getTeam()),
                    'uid' => $value->getUid(),
                 ];
                $content[] = $person;
            }
        }

        // usort($content, function ($item1, $item2) {
        //     return $item2['team'] <=> $item1['team'];
        // });

        $this->view->assign('persons', $content);
    }

    public function showAction () {

        $uid = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('uid');

        if($uid) {
             /** @var ObjectManager $objectManager */
             $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
             /** @var PageService $pageService */
             $personRepo = $objectManager->get(\Test\Employee\Domain\Repository\PersonRepository::class);

            $person = $personRepo->findByUid($uid);
            if($person) {
                $responce = [
                    'name' => $person->getTitle(),
                    'email' => $person->getEmail(),
                    'phone' => $person->getPhone(),
                    'skills' => $this->getSkillsArr($person->getSkills()),
                    'description' => $person->getDescription(),
                    'team' => $this->getTeamName($person->getTeam()),
                    'position' => $this->getPositionName($person->getPosition()),
                ];
                echo json_encode($responce);
            }
        }
    }

    // function for displaying image and person name

    public function showImagesAction()
    {
        $content = [];
        $persons = $this->personRepository->findAll();

        if($persons){

            // Getting data from flexform
            $rows = $this->settings['imgRows'];
            $colls = 12 / $this->settings['imgColls'];

            foreach ($persons as $key => $value) {
                $person = [
                    'name' => $value->getTitle(),
                    'photo' => $this->getFigure($value->getPhoto()),
                ];
                $content[] = $person;
            }
        }

        $size = sizeof($content);
        $calc = $rows * $colls;

        if ($calc < $size) {

            $elements = [];
            // generating random elements
            for($i = 0; $i < $calc; $i++){
                $pos = rand(0, $size - 1);
                $elements[$i] = $content[$pos];
            }
            $this->view->assign('persons', $elements);
            return;
        }
        $this->view->assign('persons', $content);
    }

    protected function getFigure($img)
    {
        $figure = [];
        if (null != $img) {
            $figure['src'] = $this->cropImage($img->getOriginalResource()->getPublicUrl(), $img);
        }else{
            $figure['src'] = 'typo3conf/ext/employee/Resources/Public/Images/No-image-available.jpg';
        }
        return $figure['src'];
    }

    private function cropImage($imgPath, $img)
    {
        $image = $this->imageService->getImage($imgPath, $img, true);

        // you have to set these variables or remove if you don't need them
        $processingInstructions = [
            'maxWidth' => 400,
            'maxHeight' => 500,
        ];
        $processedImage = $this->imageService->applyProcessingInstructions($image, $processingInstructions);

        return $this->imageService->getImageUri($processedImage);
    }

    private function getTeamName($obj){
       return $obj->current()->getTitle();
    }

    private function getPositionName($obj){
       return $obj->current()->getTitle();
    }

    private function getSkillsArr($obj){
        $resp = [];
        foreach($obj as $key){
            $resp[] = $key->getTitle();
        }
       return $resp;
    }
}
