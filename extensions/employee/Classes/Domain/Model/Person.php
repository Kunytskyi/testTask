<?php

namespace Test\Employee\Domain\Model;

 use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

 class Person extends AbstractEntity
 {

     /**
      * The name of the Person
      * @var string
      **/
     protected $title = '';

     /**
      *
      * @return string $title
      */
     public function setTitle($title){
         $this->title = $title;
     }

     /**
      * @param string $title
      */
     public function getTitle(){
         return $this->title;
     }

     /**
      * The isnew of the Person
      * Checkbox for new persons
      * @var string
      **/
     protected $isnew = '';

     /**
      *
      * @return string $isnew
      */
     public function setIsnew($isnew){
         $this->isNew = $isnew;
     }

     /**
      * @param string $position
      */
     public function getIsnew(){
         return $this->isnew;
     }

     /**
      * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
      **/
     protected $photo = '';

     /**
      *
      * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $photo
      */
     public function setPhoto($photo){
         $this->photo = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
     }

     /**
      * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $photo
      */
     public function getPhoto()
     {
         return $this->photo;
     }

     /**
      *
      * @var string
      **/
     protected $phone = '';

     /**
      *
      * @return string $phone
      */
     public function setPhone($phone)
     {
         $this->phone = $phone;
     }

     /**
      *
      * @param string $phone
      */
     public function getPhone()
     {
         return $this->phone;
     }

     /**
      * @var string
      **/
     protected $email = '';

     /**
      *
      * @return string $email
      */
     public function setEmail($email)
     {
         $this->email = $email;
     }

     /**
      * @param string $email
      */
     public function getEmail()
     {
         return $this->email;
     }

     /**
      * @var string
      **/
     protected $description = '';

     /**
      *
      * @return string $description
      */
     public function setDescription($description)
     {
         $this->description = $description;
     }

     /**
      * @param string $description
      */
     public function getDescription()
     {
         return $this->description;
     }

     // /**
     //  * @var string
     //  **/
     // protected $team = '';
     //
     // /**
     //  *
     //  * @return string $team
     //  */
     // public function setTeam($team)
     // {
     //     $this->team = $team;
     // }
     //
     // /**
     //  * @param string $team
     //  */
     // public function getTeam()
     // {
     //     return $this->team;
     // }

     /**
      *
      * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Test\Employee\Domain\Model\Team>
      */
     protected $team = '';

     /**
      * Adds a team.
      *
      * @param \Test\Employee\Domain\Model\Team $title
      */
     public function addTeam(\Test\Employee\Domain\Model\Team $title)
     {
         $this->team->attach($title);
     }

     /**
      * Sets the team.
      *
      * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Test\Employee\Domain\Model\Team> $title
      */
     public function setTeam(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $title)
     {
         $this->team = $title;
     }

     /**
      *
      * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Test\Employee\Domain\Model\Team> title
      */
     public function getTeam()
     {
         return $this->team;
     }

     /**
      *
      * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Test\Employee\Domain\Model\Skills>
      */
     protected $skills = '';

     /**
      * Adds a skills.
      *
      * @param \Test\Employee\Domain\Model\Skills $title
      */
     public function addSkills(\Test\Employee\Domain\Model\Skills $title)
     {
         $this->skills->attach($title);
     }

     /**
      * Sets the skills.
      *
      * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Test\Employee\Domain\Model\Skills> $title
      */
     public function setSkills(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $title){
         $this->skills = $title;
     }

     /**
      *
      * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Test\Employee\Domain\Model\Skills> title
      */
     public function getSkills()
     {
         return $this->skills;
     }

     /**
      *
      * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Test\Employee\Domain\Model\Position>
      */
     protected $position = '';

     /**
      * Adds a position.
      *
      * @param \Test\Employee\Domain\Model\Position $title
      */
     public function addPosition(\Test\Employee\Domain\Model\Position $title)
     {
         $this->position->attach($title);
     }

     /**
      * Sets the position.
      *
      * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Test\Employee\Domain\Model\Position> $title
      */
     public function setPosition(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $title)
     {
         $this->position = $title;
     }

     /**
      *
      * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Test\Employee\Domain\Model\Position> title
      */
     public function getPosition(){
         return $this->position;
     }

     public function __construct()
     {
         //Do not remove the next line: It would break the functionality
         $this->initStorageObjects();
     }

     /**
      * Initializes all ObjectStorage properties
      * Do not modify this method!
      * It will be rewritten on each save in the extension builder
      * You may modify the constructor of this class instead.
      */
     protected function initStorageObjects()
     {
         $this->team = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
         $this->position = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
         $this->skills = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
     }
 }
