<?php

namespace Test\Employee\Domain\Model;

 use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

 class Team extends AbstractEntity
 {
     /**
      * @var \TYPO3\CMS\Extbase\Domain\Model\Person|null
      * @lazy
      */
     protected $parent = null;

     /**
      * Gets the parent category.
      *
      * @return \TYPO3\CMS\Extbase\Domain\Model\Person|null the parent category
      *
      * @api
      */
     public function getParent()
     {
         if ($this->parent instanceof \TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy) {
             $this->parent->_loadRealInstance();
         }

         return $this->parent;
     }

     /**
      * Sets the parent category.
      *
      * @param \TYPO3\CMS\Extbase\Domain\Model\Person $parent the parent category
      *
      * @api
      */

     public function setParent(\TYPO3\CMS\Extbase\Domain\Model\Team $parent)
     {
         $this->parent = $parent;
     }

     /**
      * @var string
      **/
     protected $title = '';

     /**
      *
      * @return string $title
      */
     public function setTitle($title){
         $this->title = $title;
     }

     /**
      * @param string $title
      */
     public function getTitle(){
         return $this->title;
     }

     function __construct($title = '')
     {
         $this->setTitle($title);
     }
 }
