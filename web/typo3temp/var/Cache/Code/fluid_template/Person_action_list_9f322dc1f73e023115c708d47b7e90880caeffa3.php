<?php

class Person_action_list_9f322dc1f73e023115c708d47b7e90880caeffa3 extends \TYPO3Fluid\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getLayoutName(\TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this; 
return (string) '';
}
public function hasLayout() {
return FALSE;
}
public function addCompiledNamespaces(\TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$renderingContext->getViewHelperResolver()->addNamespaces(array (
  'core' => 
  array (
    0 => 'TYPO3\\CMS\\Core\\ViewHelpers',
  ),
  'f' => 
  array (
    0 => 'TYPO3Fluid\\Fluid\\ViewHelpers',
    1 => 'TYPO3\\CMS\\Fluid\\ViewHelpers',
  ),
  'formvh' => 
  array (
    0 => 'TYPO3\\CMS\\Form\\ViewHelpers',
  ),
  'v' => 
  array (
    0 => 'FluidTYPO3\\Vhs\\ViewHelpers',
  ),
));
}

/**
 * Main Render function
 */
public function render(\TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '

      <div class="row mx-auto p-0">
          <div class="row mx-auto col-8 p-0">
              ';
// Rendering ViewHelper TYPO3Fluid\Fluid\ViewHelpers\GroupedForViewHelper
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
$output4 = '';

$output4 .= '
                  <h2 class="col-12 text-center bg-light">';
$array5 = array (
);
$output4 .= call_user_func_array( function ($var) { return (is_string($var) || (is_object($var) && method_exists($var, '__toString')) ? htmlspecialchars((string) $var, ENT_QUOTES) : $var); }, [$renderingContext->getVariableProvider()->getByPath('team', $array5)]);

$output4 .= '</h2>
                  ';
// Rendering ViewHelper TYPO3Fluid\Fluid\ViewHelpers\ForViewHelper
$renderChildrenClosure7 = function() use ($renderingContext, $self) {
$output9 = '';

$output9 .= '
                      <div class="card col-sm-6 mt-1">
                          <img data-uid="';
$array10 = array (
);
$output9 .= call_user_func_array( function ($var) { return (is_string($var) || (is_object($var) && method_exists($var, '__toString')) ? htmlspecialchars((string) $var, ENT_QUOTES) : $var); }, [$renderingContext->getVariableProvider()->getByPath('person.uid', $array10)]);

$output9 .= '" src="';
$array11 = array (
);
$output9 .= call_user_func_array( function ($var) { return (is_string($var) || (is_object($var) && method_exists($var, '__toString')) ? htmlspecialchars((string) $var, ENT_QUOTES) : $var); }, [$renderingContext->getVariableProvider()->getByPath('person.photo', $array11)]);

$output9 .= '" class="card-img-top ajax" alt="image not uploaded">
                          <div class="card-body">
                                <h5 class="card-title">';
$array12 = array (
);
$output9 .= call_user_func_array( function ($var) { return (is_string($var) || (is_object($var) && method_exists($var, '__toString')) ? htmlspecialchars((string) $var, ENT_QUOTES) : $var); }, [$renderingContext->getVariableProvider()->getByPath('person.name', $array12)]);

$output9 .= '
                                    ';
// Rendering ViewHelper TYPO3Fluid\Fluid\ViewHelpers\IfViewHelper
$renderChildrenClosure14 = function() use ($renderingContext, $self) {
$output18 = '';

$output18 .= '
                                        ';
// Rendering ViewHelper TYPO3Fluid\Fluid\ViewHelpers\ThenViewHelper
$renderChildrenClosure20 = function() use ($renderingContext, $self) {
return '
                                            <span class="badge badge-pill badge-warning">New</span>
                                        ';
};
$arguments19 = array();

$output18 .= '';

$output18 .= '
                                    ';
return $output18;
};
$arguments13 = array();
$arguments13['then'] = NULL;
$arguments13['else'] = NULL;
$arguments13['condition'] = false;
// Rendering Boolean node
// Rendering Array
$array15 = array();
$array16 = array (
);$array15['0'] = $renderingContext->getVariableProvider()->getByPath('person.isNew', $array16);

$expression17 = function($context) {return TYPO3Fluid\Fluid\Core\Parser\BooleanParser::convertNodeToBoolean($context["node0"]);};
$arguments13['condition'] = TYPO3Fluid\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(
					$expression17(
						TYPO3Fluid\Fluid\Core\Parser\SyntaxTree\BooleanNode::gatherContext($renderingContext, $array15)
					),
					$renderingContext
				);
$arguments13['__thenClosure'] = function() use ($renderingContext, $self) {
return '
                                            <span class="badge badge-pill badge-warning">New</span>
                                        ';
};

$output9 .= TYPO3Fluid\Fluid\ViewHelpers\IfViewHelper::renderStatic($arguments13, $renderChildrenClosure14, $renderingContext);

$output9 .= '
                                </h5>
                          </div>
                      </div>
                  ';
return $output9;
};
$arguments6 = array();
$arguments6['each'] = NULL;
$arguments6['as'] = NULL;
$arguments6['key'] = NULL;
$arguments6['reverse'] = false;
$arguments6['iteration'] = NULL;
$array8 = array (
);$arguments6['each'] = $renderingContext->getVariableProvider()->getByPath('personsByTeam', $array8);
$arguments6['as'] = 'person';

$output4 .= TYPO3Fluid\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments6, $renderChildrenClosure7, $renderingContext);

$output4 .= '
              ';
return $output4;
};
$arguments1 = array();
$arguments1['each'] = NULL;
$arguments1['as'] = NULL;
$arguments1['groupBy'] = NULL;
$arguments1['groupKey'] = 'groupKey';
$array3 = array (
);$arguments1['each'] = $renderingContext->getVariableProvider()->getByPath('persons', $array3);
$arguments1['as'] = 'personsByTeam';
$arguments1['groupBy'] = 'team';
$arguments1['groupKey'] = 'team';

$output0 .= TYPO3Fluid\Fluid\ViewHelpers\GroupedForViewHelper::renderStatic($arguments1, $renderChildrenClosure2, $renderingContext);

$output0 .= '
          </div>
          <div class="col-4 p-5 pt-1">
             <h6 id="name"></h6>
              <p id="email"></p>
              <p id="phone"></p>
              <p id="skills"></p>
              <p id="description"></p>
              <p id="team"></p>
              <p id="position"></p>
          </div>
      </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(\'.ajax\').click(function(){
    var personUid = $(this).data(\'uid\');
    $.post(
        \'index.php\',
        ';

$output0 .= '{eID: \'employeeDetailed\',uid: personUid}';

$output0 .= ',
        function(data){
            $("#name").html(data.name);
            $("#email").html("<b>Email: </b>" + data.email);
            $("#phone").html("<b>Phone: </b>" + data.phone);
            $("#skills").html("<b>Skills: </b>" + data.skills.join(\', \'));
            $("#description").html("<b>Description: </b>" + data.description);
            $("#team").html("<b>Team: </b>" + data.team);
            $("#position").html("<b>Position: </b>" + data.position);
        }, \'json\');
});
</script>
';

return $output0;
}


}
#