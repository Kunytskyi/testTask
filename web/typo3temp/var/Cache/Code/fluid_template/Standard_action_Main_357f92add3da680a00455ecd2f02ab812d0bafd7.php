<?php

class Standard_action_Main_357f92add3da680a00455ecd2f02ab812d0bafd7 extends \TYPO3Fluid\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getLayoutName(\TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this; 
return (string) 'Default';
}
public function hasLayout() {
return TRUE;
}
public function addCompiledNamespaces(\TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$renderingContext->getViewHelperResolver()->addNamespaces(array (
  'core' => 
  array (
    0 => 'TYPO3\\CMS\\Core\\ViewHelpers',
  ),
  'f' => 
  array (
    0 => 'TYPO3Fluid\\Fluid\\ViewHelpers',
    1 => 'TYPO3\\CMS\\Fluid\\ViewHelpers',
  ),
  'formvh' => 
  array (
    0 => 'TYPO3\\CMS\\Form\\ViewHelpers',
  ),
  'v' => 
  array (
    0 => 'FluidTYPO3\\Vhs\\ViewHelpers',
  ),
));
}

/**
 * section Main
 */
public function section_62bce9422ff2d14f69ab80a154510232fc8a9afd(\TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '

    <div class="row justify-content-md-center mt-5">
        <div class="col-md-11">
            ';
// Rendering ViewHelper FluidTYPO3\Vhs\ViewHelpers\Content\RenderViewHelper
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
return NULL;
};
$arguments1 = array();
$arguments1['column'] = NULL;
$arguments1['order'] = 'sorting';
$arguments1['sortDirection'] = 'ASC';
$arguments1['pageUid'] = 0;
$arguments1['contentUids'] = NULL;
$arguments1['sectionIndexOnly'] = false;
$arguments1['loadRegister'] = NULL;
$arguments1['render'] = true;
$arguments1['hideUntranslated'] = false;
$arguments1['limit'] = NULL;
$arguments1['slide'] = 0;
$arguments1['slideCollect'] = 0;
$arguments1['slideCollectReverse'] = false;
$arguments1['as'] = NULL;
$array3 = array (
);$arguments1['pageUid'] = $renderingContext->getVariableProvider()->getByPath('data.uid', $array3);
$arguments1['column'] = 0;

$output0 .= FluidTYPO3\Vhs\ViewHelpers\Content\RenderViewHelper::renderStatic($arguments1, $renderChildrenClosure2, $renderingContext);

$output0 .= '
        </div>
    </div>

';

return $output0;
}
/**
 * Main Render function
 */
public function render(\TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output4 = '';

$output4 .= '

';
// Rendering ViewHelper TYPO3Fluid\Fluid\ViewHelpers\LayoutViewHelper
$renderChildrenClosure6 = function() use ($renderingContext, $self) {
return NULL;
};
$arguments5 = array();
$arguments5['name'] = NULL;
$arguments5['name'] = 'Default';

$output4 .= call_user_func_array( function ($var) { return (is_string($var) || (is_object($var) && method_exists($var, '__toString')) ? htmlspecialchars((string) $var, ENT_QUOTES) : $var); }, [NULL]);

$output4 .= '

';
// Rendering ViewHelper TYPO3Fluid\Fluid\ViewHelpers\SectionViewHelper
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
$output9 = '';

$output9 .= '

    <div class="row justify-content-md-center mt-5">
        <div class="col-md-11">
            ';
// Rendering ViewHelper FluidTYPO3\Vhs\ViewHelpers\Content\RenderViewHelper
$renderChildrenClosure11 = function() use ($renderingContext, $self) {
return NULL;
};
$arguments10 = array();
$arguments10['column'] = NULL;
$arguments10['order'] = 'sorting';
$arguments10['sortDirection'] = 'ASC';
$arguments10['pageUid'] = 0;
$arguments10['contentUids'] = NULL;
$arguments10['sectionIndexOnly'] = false;
$arguments10['loadRegister'] = NULL;
$arguments10['render'] = true;
$arguments10['hideUntranslated'] = false;
$arguments10['limit'] = NULL;
$arguments10['slide'] = 0;
$arguments10['slideCollect'] = 0;
$arguments10['slideCollectReverse'] = false;
$arguments10['as'] = NULL;
$array12 = array (
);$arguments10['pageUid'] = $renderingContext->getVariableProvider()->getByPath('data.uid', $array12);
$arguments10['column'] = 0;

$output9 .= FluidTYPO3\Vhs\ViewHelpers\Content\RenderViewHelper::renderStatic($arguments10, $renderChildrenClosure11, $renderingContext);

$output9 .= '
        </div>
    </div>

';
return $output9;
};
$arguments7 = array();
$arguments7['name'] = NULL;
$arguments7['name'] = 'Main';

$output4 .= NULL;

$output4 .= '


';

return $output4;
}


}
#