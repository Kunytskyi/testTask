<?php
return 
array (
  'packageStatesConfiguration' => 
  array (
    'packages' => 
    array (
      'core' => 
      array (
        'packagePath' => 'typo3/sysext/core/',
      ),
      'extbase' => 
      array (
        'packagePath' => 'typo3/sysext/extbase/',
      ),
      'fluid' => 
      array (
        'packagePath' => 'typo3/sysext/fluid/',
      ),
      'install' => 
      array (
        'packagePath' => 'typo3/sysext/install/',
      ),
      'frontend' => 
      array (
        'packagePath' => 'typo3/sysext/frontend/',
      ),
      'fluid_styled_content' => 
      array (
        'packagePath' => 'typo3/sysext/fluid_styled_content/',
      ),
      'scheduler' => 
      array (
        'packagePath' => 'typo3/sysext/scheduler/',
      ),
      'indexed_search' => 
      array (
        'packagePath' => 'typo3/sysext/indexed_search/',
      ),
      'info' => 
      array (
        'packagePath' => 'typo3/sysext/info/',
      ),
      'info_pagetsconfig' => 
      array (
        'packagePath' => 'typo3/sysext/info_pagetsconfig/',
      ),
      'extensionmanager' => 
      array (
        'packagePath' => 'typo3/sysext/extensionmanager/',
      ),
      'lang' => 
      array (
        'packagePath' => 'typo3/sysext/lang/',
      ),
      'setup' => 
      array (
        'packagePath' => 'typo3/sysext/setup/',
      ),
      'rte_ckeditor' => 
      array (
        'packagePath' => 'typo3/sysext/rte_ckeditor/',
      ),
      'rsaauth' => 
      array (
        'packagePath' => 'typo3/sysext/rsaauth/',
      ),
      'saltedpasswords' => 
      array (
        'packagePath' => 'typo3/sysext/saltedpasswords/',
      ),
      'func' => 
      array (
        'packagePath' => 'typo3/sysext/func/',
      ),
      'wizard_crpages' => 
      array (
        'packagePath' => 'typo3/sysext/wizard_crpages/',
      ),
      'wizard_sortpages' => 
      array (
        'packagePath' => 'typo3/sysext/wizard_sortpages/',
      ),
      'about' => 
      array (
        'packagePath' => 'typo3/sysext/about/',
      ),
      'backend' => 
      array (
        'packagePath' => 'typo3/sysext/backend/',
      ),
      'belog' => 
      array (
        'packagePath' => 'typo3/sysext/belog/',
      ),
      'beuser' => 
      array (
        'packagePath' => 'typo3/sysext/beuser/',
      ),
      'context_help' => 
      array (
        'packagePath' => 'typo3/sysext/context_help/',
      ),
      'cshmanual' => 
      array (
        'packagePath' => 'typo3/sysext/cshmanual/',
      ),
      'documentation' => 
      array (
        'packagePath' => 'typo3/sysext/documentation/',
      ),
      'felogin' => 
      array (
        'packagePath' => 'typo3/sysext/felogin/',
      ),
      'filelist' => 
      array (
        'packagePath' => 'typo3/sysext/filelist/',
      ),
      'form' => 
      array (
        'packagePath' => 'typo3/sysext/form/',
      ),
      'impexp' => 
      array (
        'packagePath' => 'typo3/sysext/impexp/',
      ),
      'lowlevel' => 
      array (
        'packagePath' => 'typo3/sysext/lowlevel/',
      ),
      'recordlist' => 
      array (
        'packagePath' => 'typo3/sysext/recordlist/',
      ),
      'reports' => 
      array (
        'packagePath' => 'typo3/sysext/reports/',
      ),
      'sv' => 
      array (
        'packagePath' => 'typo3/sysext/sv/',
      ),
      'sys_note' => 
      array (
        'packagePath' => 'typo3/sysext/sys_note/',
      ),
      't3editor' => 
      array (
        'packagePath' => 'typo3/sysext/t3editor/',
      ),
      'tstemplate' => 
      array (
        'packagePath' => 'typo3/sysext/tstemplate/',
      ),
      'viewpage' => 
      array (
        'packagePath' => 'typo3/sysext/viewpage/',
      ),
      'realurl' => 
      array (
        'packagePath' => 'typo3conf/ext/realurl/',
      ),
      'news' => 
      array (
        'packagePath' => 'typo3conf/ext/news/',
      ),
      'dce' => 
      array (
        'packagePath' => 'typo3conf/ext/dce/',
      ),
      'employee' => 
      array (
        'packagePath' => 'typo3conf/ext/employee/',
      ),
      'gridelements' => 
      array (
        'packagePath' => 'typo3conf/ext/gridelements/',
      ),
      'main' => 
      array (
        'packagePath' => 'typo3conf/ext/main/',
      ),
      'vhs' => 
      array (
        'packagePath' => 'typo3conf/ext/vhs/',
      ),
    ),
    'version' => 5,
  ),
  'packageAliasMap' => 
  array (
    'core' => 'core',
    'extbase' => 'extbase',
    'fluid' => 'fluid',
    'install' => 'install',
    'frontend' => 'frontend',
    'fluid_styled_content' => 'fluid_styled_content',
    'scheduler' => 'scheduler',
    'indexed_search' => 'indexed_search',
    'info' => 'info',
    'info_pagetsconfig' => 'info_pagetsconfig',
    'extensionmanager' => 'extensionmanager',
    'lang' => 'lang',
    'setup' => 'setup',
    'rte_ckeditor' => 'rte_ckeditor',
    'rsaauth' => 'rsaauth',
    'saltedpasswords' => 'saltedpasswords',
    'func' => 'func',
    'wizard_crpages' => 'wizard_crpages',
    'wizard_sortpages' => 'wizard_sortpages',
    'about' => 'about',
    'aboutmodules' => 'about',
    'backend' => 'backend',
    'belog' => 'belog',
    'beuser' => 'beuser',
    'context_help' => 'context_help',
    'cshmanual' => 'cshmanual',
    'documentation' => 'documentation',
    'felogin' => 'felogin',
    'filelist' => 'filelist',
    'form' => 'form',
    'impexp' => 'impexp',
    'lowlevel' => 'lowlevel',
    'recordlist' => 'recordlist',
    'reports' => 'reports',
    'sv' => 'sv',
    'sys_note' => 'sys_note',
    't3editor' => 't3editor',
    'tstemplate' => 'tstemplate',
    'viewpage' => 'viewpage',
    'realurl' => 'realurl',
    'typo3-ter/realurl' => 'realurl',
    'news' => 'news',
    'typo3-ter/news' => 'news',
    'dce' => 'dce',
    'typo3-ter/dce' => 'dce',
    'gridelements' => 'gridelements',
    'typo3-ter/gridelements' => 'gridelements',
    'vhs' => 'vhs',
    'typo3-ter/vhs' => 'vhs',
  ),
  'loadedExtArray' => 
  array (
    'core' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/core/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/core/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3/sysext/core/ext_tables.sql',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'extbase' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/extbase/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/extbase/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3/sysext/extbase/ext_tables.sql',
      'ext_typoscript_setup.txt' => '/var/www/localhost/www/html/web/typo3/sysext/extbase/ext_typoscript_setup.txt',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'fluid' => 
    array (
      'type' => 'S',
      'ext_typoscript_setup.txt' => '/var/www/localhost/www/html/web/typo3/sysext/fluid/ext_typoscript_setup.txt',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'install' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/install/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/install/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'frontend' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/frontend/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/frontend/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3/sysext/frontend/ext_tables.sql',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'fluid_styled_content' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/fluid_styled_content/ext_localconf.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'scheduler' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/scheduler/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/scheduler/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3/sysext/scheduler/ext_tables.sql',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'indexed_search' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/indexed_search/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/indexed_search/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3/sysext/indexed_search/ext_tables.sql',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'info' => 
    array (
      'type' => 'S',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/info/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'info_pagetsconfig' => 
    array (
      'type' => 'S',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/info_pagetsconfig/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'extensionmanager' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/extensionmanager/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/extensionmanager/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3/sysext/extensionmanager/ext_tables.sql',
      'ext_tables_static+adt.sql' => '/var/www/localhost/www/html/web/typo3/sysext/extensionmanager/ext_tables_static+adt.sql',
      'ext_typoscript_setup.txt' => '/var/www/localhost/www/html/web/typo3/sysext/extensionmanager/ext_typoscript_setup.txt',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'lang' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/lang/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/lang/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'setup' => 
    array (
      'type' => 'S',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/setup/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'rte_ckeditor' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/rte_ckeditor/ext_localconf.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'rsaauth' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/rsaauth/ext_localconf.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3/sysext/rsaauth/ext_tables.sql',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'saltedpasswords' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/saltedpasswords/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/saltedpasswords/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'func' => 
    array (
      'type' => 'S',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/func/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'wizard_crpages' => 
    array (
      'type' => 'S',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/wizard_crpages/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'wizard_sortpages' => 
    array (
      'type' => 'S',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/wizard_sortpages/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'about' => 
    array (
      'type' => 'S',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/about/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'backend' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/backend/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/backend/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'belog' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/belog/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/belog/ext_tables.php',
      'ext_typoscript_setup.txt' => '/var/www/localhost/www/html/web/typo3/sysext/belog/ext_typoscript_setup.txt',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'beuser' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/beuser/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/beuser/ext_tables.php',
      'ext_typoscript_setup.txt' => '/var/www/localhost/www/html/web/typo3/sysext/beuser/ext_typoscript_setup.txt',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'context_help' => 
    array (
      'type' => 'S',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/context_help/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'cshmanual' => 
    array (
      'type' => 'S',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/cshmanual/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'documentation' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/documentation/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/documentation/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'felogin' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/felogin/ext_localconf.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3/sysext/felogin/ext_tables.sql',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'filelist' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/filelist/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/filelist/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'form' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/form/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/form/ext_tables.php',
      'ext_typoscript_setup.txt' => '/var/www/localhost/www/html/web/typo3/sysext/form/ext_typoscript_setup.txt',
      'ext_icon' => 'Resources/Public/Icons/Extension.svg',
    ),
    'impexp' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/impexp/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/impexp/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3/sysext/impexp/ext_tables.sql',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'lowlevel' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/lowlevel/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/lowlevel/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'recordlist' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/recordlist/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/recordlist/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'reports' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/reports/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/reports/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'sv' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/sv/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/sv/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'sys_note' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/sys_note/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/sys_note/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3/sysext/sys_note/ext_tables.sql',
      'ext_typoscript_constants.txt' => '/var/www/localhost/www/html/web/typo3/sysext/sys_note/ext_typoscript_constants.txt',
      'ext_typoscript_setup.txt' => '/var/www/localhost/www/html/web/typo3/sysext/sys_note/ext_typoscript_setup.txt',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    't3editor' => 
    array (
      'type' => 'S',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3/sysext/t3editor/ext_localconf.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'tstemplate' => 
    array (
      'type' => 'S',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/tstemplate/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'viewpage' => 
    array (
      'type' => 'S',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3/sysext/viewpage/ext_tables.php',
      'ext_icon' => 'Resources/Public/Icons/Extension.png',
    ),
    'realurl' => 
    array (
      'type' => 'L',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3conf/ext/realurl/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3conf/ext/realurl/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3conf/ext/realurl/ext_tables.sql',
      'ext_typoscript_setup.txt' => '/var/www/localhost/www/html/web/typo3conf/ext/realurl/ext_typoscript_setup.txt',
      'ext_icon' => 'Resources/Public/Icons/Extension.svg',
    ),
    'news' => 
    array (
      'type' => 'L',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3conf/ext/news/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3conf/ext/news/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3conf/ext/news/ext_tables.sql',
      'ext_typoscript_setup.txt' => '/var/www/localhost/www/html/web/typo3conf/ext/news/ext_typoscript_setup.txt',
      'ext_icon' => 'Resources/Public/Icons/Extension.svg',
    ),
    'dce' => 
    array (
      'type' => 'L',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3conf/ext/dce/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3conf/ext/dce/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3conf/ext/dce/ext_tables.sql',
      'ext_typoscript_setup.txt' => '/var/www/localhost/www/html/web/typo3conf/ext/dce/ext_typoscript_setup.txt',
      'ext_icon' => 'ext_icon.png',
    ),
    'employee' => 
    array (
      'type' => 'L',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3conf/ext/employee/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3conf/ext/employee/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3conf/ext/employee/ext_tables.sql',
      'ext_icon' => '',
    ),
    'gridelements' => 
    array (
      'type' => 'L',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3conf/ext/gridelements/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3conf/ext/gridelements/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3conf/ext/gridelements/ext_tables.sql',
      'ext_icon' => 'ext_icon.svg',
    ),
    'main' => 
    array (
      'type' => 'L',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3conf/ext/main/ext_localconf.php',
      'ext_tables.php' => '/var/www/localhost/www/html/web/typo3conf/ext/main/ext_tables.php',
      'ext_tables.sql' => '/var/www/localhost/www/html/web/typo3conf/ext/main/ext_tables.sql',
      'ext_tables_static+adt.sql' => '/var/www/localhost/www/html/web/typo3conf/ext/main/ext_tables_static+adt.sql',
      'ext_icon' => 'ext_icon.png',
    ),
    'vhs' => 
    array (
      'type' => 'L',
      'ext_localconf.php' => '/var/www/localhost/www/html/web/typo3conf/ext/vhs/ext_localconf.php',
      'ext_icon' => 'ext_icon.gif',
    ),
  ),
  'composerNameToPackageKeyMap' => 
  array (
  ),
  'packageObjectsCacheEntryIdentifier' => 'PackageObjects_5b59a4aba034d055502748',
);
#